# General setup notes:

needs tidying up but for now focusing on the non obvious, namely pgcli and powerline

## Python:

Main python section

### yapf

config file needs to be symlinked into `~/.config/yapf`, IE:

`ln -s <repo_root>/python/yapf/style.yapf $HOME/.config/yapf/style`

possible alternative:

you can copy the file into a project root and if it is found first then those settings are applied.

## Powerline:

Powerline theme settings must be copied from `<powerline_install_location>/site-packages/config_files/themes`
into `~/.config/powerline/themes` and these files will always be merged, so custom settings naturally go
under your `~/config` path similar to how `nvim` config does.

The above is the general concept, but for our purposes we will just symlink in our custom config, which is currently

`<repo_root>/powerline/themes/shell/default.json` into `~/.config/themes/shell/default.json`, the later path must be created first
`mkdir -p ~/.config/powerline/themes/shell` in order to be able to symlink into that location.

Same applies to tmux theme.. just follow the path as set in this repo reflected in `.config/powerline`


## Pgcli:

similar to the above (without two config files being merged), symlink `<repo_root>/pgcli/config` into `~/.config/pgcli/config`


## Ipython

files need to be put top level in `~/.ipython`.. symlink `profile_default/ipython_config.py` into former directory for a complete
path of `~/.ipython/profile_default/ipython_config.py`


## Tmux:

Bindings must be sourced from `~/.tmux.conf` and these are located under `<repo_root>/powerline/bindings/tmux/powerline.conf`
