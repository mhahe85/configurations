# --- Notes:
# Seems variable expansion with default values will not work in tmux config
# while env vars without defaults are fine in the format of `$EXAMPLEVAR`
# and not `${EXAMPLEVAR:-mydefaultvalue}`

# Plugins:

## TPM -- part of instructions
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'

## tmux-resurrect -- save sessions and state
set -g @plugin 'tmux-plugins/tmux-resurrect'


## tmux-sidebar -- directory listing of current working directory
set -g @plugin 'tmux-plugins/tmux-sidebar'

# --- Shell Settings:
# Set the shell explicitly rather than relying on the default
# $SHELL environment variable, if $SHELL is empty defaults
# to `/bin/sh`... these examples are left to fiddle with
#set-option -g default-shell "$TMUX_SHELL_OVERRIDE"
# set-option -g default-shell /bin/bash
# set-option -g default-shell /bin/zsh

## --- Powerline
# make sure daemon running before calling config
run-shell "powerline-daemon -q"
# env var 'pl_py_ver' is exported from shell configs under top level python module
# pl_py_ver stands for powerline_python_version
source "$HOME/.local/venvs/powerline-status/lib/python${pl_py_ver}/site-packages/powerline/bindings/tmux/powerline.conf"


set-window-option -g mode-keys vi
bind-key -T copy-mode-vi v send-keys -X begin-selection
bind-key -T copy-mode-vi y send-keys -X copy-selection

set-option -sg escape-time 10
set-option -g default-terminal "screen-256color"

# Force Tmux to use 24bit colour
set-option -ga terminal-overrides ",*256col*:Tc"

# move between splits
# check prefix ? for binds and this overlaped with below
# caused j to only jump once when using multiple times
unbind -T prefix J
# Now i can keep jumping with only one prefix use on down (j)
bind -r -T prefix h select-pane -L
bind -r -T prefix l select-pane -R
bind -r -T prefix k select-pane -U
bind -r -T prefix j select-pane -D

# resize splits
bind -r -T prefix M-h resize-pane -L 5
bind -r -T prefix M-l resize-pane -R 5
bind -r -T prefix M-k resize-pane -U 5
bind -r -T prefix M-j resize-pane -D 5

# reload config file -- There is a default for this
bind -T prefix r source-file ~/.tmux.conf

# Switch to previous session
bind -T prefix B switch-client -l

# mouse control (clickable windows, resizable panes)
set -g mouse on

# window and pane base indices
set -g base-index 0
setw -g pane-base-index 0

# Plugins -- end [NEEDED]
## TPM -- part of instructions
## Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'
