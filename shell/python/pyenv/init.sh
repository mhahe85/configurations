source $config_root/python/pyenv/aliases.sh
source $config_root/python/pyenv/exports.sh

if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi

