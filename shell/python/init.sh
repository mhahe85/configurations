# top level
source $config_root/python/aliases.sh
source $config_root/python/exports.sh

# sub modules
source $config_root/python/powerline/init.sh
source $config_root/python/pyenv/init.sh
