# /usr/local/bin used by homebrew to symlink executables to
# homebrew exe path needs to be last to avoid nvm/pyenv
# shims not getting picked up.
# IE: homebrew installed python will be picked up
# and not the global or local set by pyenv
export PATH=$PATH:/usr/local/bin
