# set shell to use vim key bindings
bindkey -v

# Allow to edit shell in [n]vim
autoload -U edit-command-line
zle -N edit-command-line

# Rebind edit in vim to ctrl+f because
# zsh supports visual selection in the shell
# so v already used
bindkey -M vicmd "^f" edit-command-line

# Single history between sessions:
# -- can't have this when share_history active
#setopt inc_append_history

# When calling a line out of history do not execute
# immediately and wait for 2nd carriage return
setopt hist_verify
# Save datetime and execution time
setopt extended_history
# share history between sessions
setopt share_history

# Allow for bash like interactive comments
setopt interactivecomments
