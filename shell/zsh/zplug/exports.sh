uname | grep -qi linux
is_linux=$?

if [ ${is_linux} -eq 0 ]
then
  export ZPLUG_HOME=$HOME/bare_installs/zplug
else
  export ZPLUG_HOME=/usr/local/opt/zplug
fi
