# Allow for vim mode switching to be snappy, set to 1 sec
export KEYTIMEOUT=1
export HISTFILE=$HOME/.zsh_history
export SAVEHIST=1000000
