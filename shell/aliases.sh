# vi: ft=zsh

# Built-in
alias c="clear"
alias rg='rg --color=always'
alias rgc='rg -C 4 --color=always'
alias grep='grep --color=always'
alias fgrep='fgrep --color=always'
alias egrep='egrep --color=always'
alias ls="ls -G"
alias l="ls -G"
alias ll="ls -lGah"
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."

# External
alias g="git"
alias gs="git status"
alias gcom="git commit"
alias gcho="git checkout"
alias gchp="git cherry-pick"
alias ga="git add"
alias gai="git add -i"
alias gap="git add -p"
alias gl="git log --oneline"
alias glg="git log --oneline --graph"
alias gll="git log"
alias glll="git log --oneline --graph"
alias gb="git branch"
alias gd="git diff"
alias gdc="git diff --cached"
alias gr="git rebase"

alias dco="docker-compose"
alias d="docker"

alias tmux='tmux -2'
alias tmx='tmux -L mysocket -S /home/mhahe/.tmux_sockets/mysocket'

alias glances='glances -1'
#alias glances='glances --theme-white -1'
