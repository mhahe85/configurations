#!/bin/bash

INSTALLDIR=$HOME/opt/vim
DOWNLOADDIR=$HOME/vimsrc

#THIS ONE MIGHT BE FOR OLD VERSIONS OF VIM
###sudo apt-get install gtk2-engines-pixbuf
#??????????????????????????????????????????




mkdir $DOWNLOADDIR

sudo apt-get build-dep vim

git clone https://github.com/vim/vim.git $DOWNLOADDIR

cd $DOWNLOADDIR

./configure --enable-python3interp --enable-pythoninterp \
            --with-features=huge --prefix=$INSTALLDIR

make && make install

mkdir -p $HOME/bin

cd $HOME/bin

ln -s $INSTALLDIR/bin/vim

#####INSTALL PATHOGEN

mkdir -p ~/.vim/autoload ~/.vim/bundle
cd ~/.vim/autoload
git clone https://github.com/tpope/vim-pathogen.git
cp vim-pathogen/autoload/pathogen.vim .
rm -r vim-pathogen


#####INSTALL WOMBAT THEME
mkdir -p ~/.vim/colors && cd ~/.vim/colors
git clone https://github.com/michalbachowski/vim-wombat256mod.git
cp vim-wombat256mod/colors/wombat256mod.vim .
rm -r vim-wombat256mod
#POSSIBLE NEED FOR export TERM=xterm-256color


#####INSTALL POWERLINE
sudo pip install -U powerline-status
#*****LATEST AND GREATEST WAY POSSIBLY?? or maybe not
#pip install --user git+git://github.com/powerline/powerline
#####INSTALL CTRLP -- fuzzy file finder
cd ~/.vim
git clone https://github.com/kien/ctrlp.vim.git bundle/ctrlp.vim

#####INSTALL JEDI
cd ~/.vim/bundle/ && git clone --recursive https://github.com/davidhalter/jedi-vim.git

#####INSTALL XX
#####INSTALL XX


# *********** JAVASCRIPT
# # Follow guide from:
# # https://davidosomething.com/blog/vim-for-javascript/
# # Assuming pathogen installed
# cd .vim/bundle
# # better syntax highlighting
# git clone https://github.com/othree/yajs.vim.git
# # better indenting
# git clone https://github.com/gavocanov/vim-js-indent.git
# # BELOW REQUIRE NODE AND NPM INSTALLED AND GLOBALLY ACCESSABLE
# # code analisys -- code completion
# git clone https://github.com/ternjs/tern_for_vim.git
# cd tern_for_vim
# npm install
# # STUCK HERE -- NEED TO ENABLE TERNJS AND CONFIGURE PROPERLY
