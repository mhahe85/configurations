#!/usr/bin/node

var pr1 = new Promise((resolve, reject) => {
  // Async Part
  setTimeout(()=>{
    resolve('pass');
    reject('error1');
  }, 5000);
});

pr1.then(value => {
  console.log(value);
  return new Promise((resolve, reject)=>{
    setTimeout(()=>{
      reject('error2');
    }, 10000);
  });
}).then(value => {
  console.log(value);
  return new Promise((resolve, reject)=>{
    setTimeout(()=>{
      reject('error3');
    }, 0);
  });
}).catch(error => {
  console.log(error);
});



pAll = [
  (function(time){
    return new Promise((resolve, reject)=>{
      setTimeout(()=>{
        console.log(time);
        resolve(time);
      }, time);
    });
  })(5000),
  (function(time){
    return new Promise((resolve, reject)=>{
      setTimeout(()=>{
        console.log(time);
        resolve(time);
      }, time);
    });
  })(1000),
  (function(time){
    return new Promise((resolve, reject)=>{
      setTimeout(()=>{
        console.log(time);
        resolve(time);
      }, time);
    });
  })(3000)
];

Promise.all(pAll).then(function(value) {
  console.log(value);
});
